+++
title = "About"
author = ["Ben Ide"]
lastmod = 2018-05-09T11:18:16-04:00
draft = false
weight = 15
noauthor = true
nocomment = true
nodate = true
nopaging = true
noread = true
[menu.main]
  identifier = "about"
+++

_Universal Optimization_ is a repository of ideas that your co-blogging authors find themselves wanting to share. We find that our way of interpreting the world is both similar and non-standard, thus, a blog was born.

We'll be posting about mathematics, academia, philosophy, or anything else that strikes us. The unifying theme is that we tend to see everything as some sort of optimization process. That doesn't mean we'll explicitly spell out the process in every post, rather that this can be understood implicitly as our style of interpreting the thing we're writing about.

Your co-bloggers are two mathematicians early in their careers:


## Ben {#ben}


## Libby {#libby}
