+++
title = "Advice on Applying for Graduate School"
author = ["Libby Taylor"]
date = 2018-05-10
lastmod = 2018-05-10T11:28:04-04:00
tags = ["grad school"]
draft = false
+++

<!--more-->

As a (recent former) undergraduate at Georgia Tech who is soon headed to grad school at Stanford, I’ve had about half a dozen undergraduates approach me in the last week to ask for advice on grad schools, fellowship applications, etc., and what they should be doing to be successful in those areas.  There seems to be a dearth of advice for grad-school-bound undergrads, so I've written up a list of miscellaneous pieces of advice that I have received that others might also find useful.  The headings are organized in roughly chronological order.


## Preparing for Grad School {#preparing-for-grad-school}

1.  Take graduate courses as soon as you can.  It’s preferable to get a B in a graduate course than an A in an undergraduate one, since you’ll learn the subject far more thoroughly taking the graduate version.  In addition, if you want to get into a top grad program, having taken some graduate courses is almost a prerequisite for admission to such places.

2.  Volunteer for department events whenever you can, and keep a list of which things you’ve volunteered for.  (More on this in the fellowships section.)

3.  Get started on research earlier than you think you should.  After your second year, you’ve probably seen enough things to be able to approach some kind of research problem.  The only prerequisites for research are a good understanding of proofs and knowledge in at least one of graph theory/combinatorics, analysis, and algebra.  Most undergrad research projects are in combinatorics, graph theory, and tropical geometry, because those fields have a much lower barrier to entry in terms of coursework and background knowledge.  (The high barrier to entry is why you don’t see undergrads doing research in, for example, homotopy type theory or algebraic geometry.)

4.  Get to know some of your professors.  Most of them don’t bite (I promise!) and are happy to help you if you ask.  Even if you’re not having trouble in class, go to their office hours anyway to chat about course material.  You’ll learn topics much more thoroughly that way, and often professors will tell you some interesting things related to the course that you wouldn’t have seen otherwise.  Plus, this won’t hurt when it comes time to ask for letters of recommendation for grad schools and fellowships.  Speaking of which:


## Fellowship Applications {#fellowship-applications}

For the NSF graduate fellowship: the NSF review panel rates all applicants on two criteria, which are intellectual merit and broader impacts.  Nearly everyone gets high scores on the intellectual merit section, which considers your GPA, course load, research, and publications (if you have any).  The difference in applicants mostly comes from the broader impacts section, which has to do with your demonstrated potential for becoming a positive force in the scientific community.  The sorts of things they look for here are whether you’ve been active in your department and in outreach.  If you’ve done activities that are intended to promote minorities in STEM fields, then that’s a great thing to include in your application.

If you want to be a competitive applicant for the NSF fellowship, you should start working on things for your broader impacts section now, not later.  This is something you really do need to plan for.

If you've worked with a high school math competition at your university, or with local math circles, that's a great thing to talk about.  If you've been involved with the AWM at all, that's also good.


## Applying to Grad Schools {#applying-to-grad-schools}

1.  The summer before your senior year, to talk to a professor you know, whether that's a research mentor or someone you've taken some classes from, and ask them for suggestions about which programs to apply to.  Someone who's worked with you can probably give you a good idea of the caliber of schools for which you'll be competitive, as well as which schools would be a good fit.  Keep in mind that it's not always a good decision to go to the most highly ranked place you got in.  You also need to consider the availability of advisors, whether there's a strong group of students in the area you're considering, and the department culture as a whole. Some departments are known for being much more “mathematically introverted” than others. If you're the kind of student who learns best from reading on their own, then that would be a great fit.  If, like me, you learn best from talking with other people, then a more mathematically extroverted place might be a better option.

2.  If you know what kind of math you want to do, ask a professor in that area for suggestions on potential advisors at various schools.  Then go email those people and ask if they are taking students.  The way I did this was to say that, “I'm so-and-so's undergrad research student at Georgia Tech, and will be applying to your school this fall. So-and-so suggested that I email you to ask if you'll be taking students next year.” This made me more identifiable as the student of someone they know, rather than some random undergrad emailing them.  Best case scenario, you get someone interested in having you and they'll put in a good word for you in admissions. Worst case scenario, they ignore your email.  If that happens, so what?  It's not like they're going to remember five years down the road that you emailed them and bothered them.  In some cases they'll tell you that they'll be on sabbatical the year you start, or that they already have ten students and don't have room for any more.  Both of these are good things to know, since that person is out as a potential advisor.
