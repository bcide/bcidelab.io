+++
title = "Weak Pescatarianism"
author = ["Benjamin Ide"]
lastmod = 2018-08-13T17:30:21-04:00
tags = ["diet"]
categories = ["life"]
draft = false
+++

In the pursuit of optimizing everything we inevitably need to think about food. There are many dietary questions, but here I will stick to efficiency at the societal level.

<!--more-->

I've settled on what I call _weak pescatarianism_ as the optimum dietary restriction to put the right kind of pressure on society. Pescatarians are essentially vegetarians that still eat fish. By _weak_, I mean that it's ok to eat meat when it's given to you, at a dinner party for example, but that you shouldn't outright spend money on meat because [money is the unit of caring](https://www.lesswrong.com/posts/ZpDnRCeef2CLEFeKM/money-the-unit-of-caring).

I've seen many arguments for vegetarianism, but there is a simple argument that seems overlooked. In a meat based diet, resources (land, fossil fuels, etc.) are being used to grow food to feed to food. This is clearly wasteful. Wild caught seafood doesn't fall prey to this problem.

This explains why I've decided to restrict to pescatarianism, but not why I'm ok with only following it weakly. One reason is that I'm weak willed, and if I want to actually follow through, this is the best I can do. Otherwise, I would give up.

Further, it is much more socially palatable. The cultural meme of the annoying vegetarian is pervasive. This sidesteps that problem. However, I suspect, due to the first point, that this is just motivated reasoning and might be flawed.

As supporters of [effective altruism](https://www.effectivealtruism.org/) we have seen many moral arguments related to the suffering caused by factory farming. These aren't bad arguments, _per se_, but they require fuzzy utilitarian calculations, and feel much less intuitive. It is entirely intuitive that feeding food to your food is an inefficient use of food.
