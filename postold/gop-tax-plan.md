---
title: Tax changes for graduate students under the Tax Cuts and Jobs Act
date: 2017-11-05
tags: ["GOP", "taxes", "graduate school"]
---

#### UPDATE

 - I incorrectly included FICA taxes. Thanks, reddit users /u/SweaterFish and /u/easasd.
 - I didn't initially realize the changes to the standard deduction and personal exemptions. Thanks, /u/ant_guy.

If you read this before these updates, *please* keep [anchoring](https://en.wikipedia.org/wiki/Anchoring) in mind when reevaluating. For me, the outcome remains the same, but it's possible that this change could change your mind about this topic even though it doesn't for me.

### Information and references

H.R.1 -- 155th Congress (Tax Cuts and Jobs Act) [1] proposes changes to the US Tax Code that threatens to destroy the finances of STEM graduate students nationwide. The offending provision, 1204(a)(3), strikes section 117(d) [2] of the US Tax Code. This means that under the proposal, tuition waivers are considered taxable income.

For graduate students, this means an increase of thousands of dollars in owed federal taxes. Below I show a calculation for my own situation. The short of it is this: My federal taxes increase from ~7.5% of my income to ~31%. I will owe about $6300 more in federal taxes under this legislation. Like many other STEM students, my choices would be limited to taking on significant debt or quitting my program entirely.

### Calculating your taxes

For tax year 2017, the following table lists the marginal tax rates on taxable income (using current US Tax Code):

| Marginal Tax | Single        | Married Filing Jointly | Married Filing Separately | Head of Household |
|-------------:|:--------------|:-----------------------|:--------------------------|:------------------|
|          0.1 | up to $9325   | up to $18650           | up to $9325               | up to $13350      |
|         0.15 | up to $37950  | up to $75900           | up to $37950              | up to $50800      |
|         0.25 | up to $91900  | up to $153100          | up to $76550              | up to $131200     |
|         0.28 | up to $191650 | up to $233350          | up to $116675             | up to $212500     |
|         0.33 |               |                        |                           | etc...            |
|         0.35 |               |                        |                           |                   |
|        0.396 |               |                        |                           |                   |

Under the Tax Cuts and Jobs Act, the marginal tax rates become:

| Marginal Tax | Single        | Married Filing Jointly | Married Filing Separately | Head of Household |
|-------------:|:--------------|:-----------------------|:--------------------------|:------------------|
|         0.12 | up to $45000  | up to $90000           | up to $45000              | up to $67500      |
|         0.25 | up to $130000 | up to $260000          | up to $130000             | up to $195000     |
|         0.35 |               |                        |                           | etc...            |
|        0.396 |               |                        |                           |                   |

 - The standard deduction for tax year 2017 is $6350 under the current code.
 - The personal exemption for tax year 2017 is $4050 under the current code.
 - *UPDATE* : The GOP plan gets rid of the personal exemption and increases the standard deduction to $12,200.
 - *UPDATE* : FICA does not apply to (most?) graduate assistants
 - Under the Tax Cuts and Jobs Act, tuition waivers are no longer considered exempt and must be included in taxable income.

### Example

I make $27,000/year and have a tuition waiver worth $42,021/year and I take the standard deduction.

#### Under current law
 - Income tax
$$\text{income} - \text{standard deduction} - \text{personal exemption} = \$27000-\$6350-\$4050 = \$16600$$
$$\text{federal income tax} = 0.1\cdot\$9325 + 0.15\cdot(\$16600-\$9325) = \$2023.75$$
As a percentage of my income of $27,000, this is 7.5%.

#### Under the GOP's proposal
 - Income tax
$$\text{income with tuition} - \text{standard deduction} - \text{personal exemption} = \$69021-\$12200 = \$56821$$
$$\text{federal income tax} = 0.12\cdot\$45000 + 0.25\cdot(\$56821-\$45000) = \$8355.25$$
As a percentage of my income of $27,000, this is 31%.

[1]: https://www.congress.gov/bill/115th-congress/house-bill/1/text
[2]: https://www.law.cornell.edu/uscode/text/26/117
